// +----------------------------------------------------------------------
// | EasyGoAdmin敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2025 深圳EasyGoAdmin研发中心
// +----------------------------------------------------------------------
// | Licensed Apache-2.0 EasyGoAdmin并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.easygoadmin.vip
// +----------------------------------------------------------------------
// | Author: @半城风雨 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package router

import (
	"easygoadmin/app/controller"
	middleware2 "easygoadmin/app/middleware"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

// 注册路由
func RegisterRouter(e *echo.Echo) {
	// 日志中间件
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	// 跨域处理中间件
	e.Use(middleware.CORS())
	// 登录验证中间件
	e.Use(middleware2.CheckLogin)
	// 登录
	login := e.Group("")
	{
		login.GET("/", controller.Login.Login)
		login.POST("/login", controller.Login.Login)
		login.GET("/captcha", controller.Login.Captcha)
		login.PUT("/updateUserInfo", controller.Index.UpdateUserInfo)
		login.PUT("/updatePwd", controller.Index.UpdatePwd)
		login.GET("/logout", controller.Index.Logout)
	}

	// 主页
	index := e.Group("/index")
	{
		index.GET("/menu", controller.Index.Menu)
		index.GET("/user", controller.Index.User)
	}

	// 文件上传
	upload := e.Group("/upload")
	{
		upload.POST("/uploadImage", controller.Upload.UploadImage)
	}

	// 职级管理
	level := e.Group("/level")
	{
		level.GET("/list", controller.Level.List)
		level.GET("/detail/:id", controller.Level.Detail)
		level.POST("/add", controller.Level.Add)
		level.PUT("/update", controller.Level.Update)
		level.DELETE("/delete/:id", controller.Level.Delete)
		level.PUT("/status", controller.Level.Status)
		level.GET("/getLevelList", controller.Level.GetLevelList)
		level.GET("/exportExcel", controller.Level.ExportExcel)
		level.POST("/importExcel", controller.Level.ImportExcel)
		level.GET("/downloadExcel", controller.Level.DownloadExcel)
	}

	// 岗位管理
	position := e.Group("/position")
	{
		position.GET("/list", controller.Position.List)
		position.GET("/detail/:id", controller.Position.Detail)
		position.POST("/add", controller.Position.Add)
		position.PUT("/update", controller.Position.Update)
		position.DELETE("/delete/:id", controller.Position.Delete)
		position.PUT("/status", controller.Position.Status)
		position.GET("/getPositionList", controller.Position.GetPositionList)
	}

	/* 角色路由 */
	role := e.Group("role")
	{
		role.GET("/list", controller.Role.List)
		role.GET("/detail/:id", controller.Role.Detail)
		role.POST("/add", controller.Role.Add)
		role.PUT("/update", controller.Role.Update)
		role.DELETE("/delete/:id", controller.Role.Delete)
		role.PUT("/status", controller.Role.Status)
		role.GET("/getRoleList", controller.Role.GetRoleList)
	}

	/* 角色菜单权限 */
	roleMenu := e.Group("rolemenu")
	{
		roleMenu.GET("/index/:roleId", controller.RoleMenu.Index)
		roleMenu.POST("/save", controller.RoleMenu.Save)
	}

	/* 部门管理 */
	dept := e.Group("dept")
	{
		dept.GET("/list", controller.Dept.List)
		dept.GET("/detail/:id", controller.Dept.Detail)
		dept.POST("/add", controller.Dept.Add)
		dept.PUT("/update", controller.Dept.Update)
		dept.DELETE("/delete/:id", controller.Dept.Delete)
		dept.GET("/getDeptList", controller.Dept.GetDeptList)
	}

	/* 用户管理 */
	user := e.Group("user")
	{
		user.GET("/list", controller.User.List)
		user.GET("/detail/:id", controller.User.Detail)
		user.POST("/add", controller.User.Add)
		user.PUT("/update", controller.User.Update)
		user.DELETE("/delete/:id", controller.User.Delete)
		user.PUT("/status", controller.User.Status)
		user.PUT("/resetPwd", controller.User.ResetPwd)
		user.GET("/checkUser/{username:string}", controller.User.CheckUser)
	}

	/* 菜单管理 */
	menu := e.Group("menu")
	{
		menu.GET("/list", controller.Menu.List)
		menu.GET("/detail/:id", controller.Menu.Detail)
		menu.POST("/add", controller.Menu.Add)
		menu.PUT("/update", controller.Menu.Update)
		menu.DELETE("/delete/:id", controller.Menu.Delete)
	}

	/* 友链管理 */
	link := e.Group("link")
	{
		link.GET("/list", controller.Link.List)
		link.GET("/detail/:id", controller.Link.Detail)
		link.POST("/add", controller.Link.Add)
		link.PUT("/update", controller.Link.Update)
		link.DELETE("/delete/:id", controller.Link.Delete)
		link.PUT("/status", controller.Link.Status)
	}

	/* 城市管理 */
	city := e.Group("city")
	{
		city.GET("/list", controller.City.List)
		city.GET("/detail/:id", controller.City.Detail)
		city.POST("/add", controller.City.Add)
		city.PUT("/update", controller.City.Update)
		city.DELETE("/delete/:id", controller.City.Delete)
		city.POST("/getChilds", controller.City.GetChilds)
	}

	/* 通知管理 */
	notice := e.Group("notice")
	{
		notice.GET("/list", controller.Notice.List)
		notice.GET("/detail/:id", controller.Notice.Detail)
		notice.POST("/add", controller.Notice.Add)
		notice.PUT("/update", controller.Notice.Update)
		notice.DELETE("/delete/:id", controller.Notice.Delete)
		notice.PUT("/status", controller.Notice.Status)
	}

	/* 会员等级 */
	memberlevel := e.Group("memberlevel")
	{
		memberlevel.GET("/list", controller.MemberLevel.List)
		memberlevel.GET("/detail/:id", controller.MemberLevel.Detail)
		memberlevel.POST("/add", controller.MemberLevel.Add)
		memberlevel.PUT("/update", controller.MemberLevel.Update)
		memberlevel.DELETE("/delete/:id", controller.MemberLevel.Delete)
		memberlevel.GET("/getMemberLevelList", controller.MemberLevel.GetMemberLevelList)
	}

	/* 会员管理 */
	member := e.Group("member")
	{
		member.GET("/list", controller.Member.List)
		member.GET("/detail/:id", controller.Member.Detail)
		member.POST("/add", controller.Member.Add)
		member.PUT("/update", controller.Member.Update)
		member.DELETE("/delete/:id", controller.Member.Delete)
		member.PUT("/status", controller.Member.Status)
	}

	/* 字典管理 */
	dict := e.Group("dict")
	{
		dict.GET("/list", controller.Dict.List)
		dict.GET("/detail/:id", controller.Dict.Detail)
		dict.POST("/add", controller.Dict.Add)
		dict.PUT("/update", controller.Dict.Update)
		dict.DELETE("/delete/:id", controller.Dict.Delete)
	}

	/* 字典项管理 */
	dictdata := e.Group("dictdata")
	{
		dictdata.GET("/list", controller.DictData.List)
		dictdata.GET("/detail/:id", controller.DictData.Detail)
		dictdata.POST("/add", controller.DictData.Add)
		dictdata.PUT("/update", controller.DictData.Update)
		dictdata.DELETE("/delete/:id", controller.DictData.Delete)
	}

	/* 配置管理 */
	config := e.Group("config")
	{
		config.GET("/list", controller.Config.List)
		config.GET("/detail/:id", controller.Config.Detail)
		config.POST("/add", controller.Config.Add)
		config.PUT("/update", controller.Config.Update)
		config.DELETE("/delete/:id", controller.Config.Delete)
	}

	/* 配置项管理 */
	configdata := e.Group("configdata")
	{
		configdata.GET("/list", controller.ConfigData.List)
		configdata.GET("/detail/:id", controller.ConfigData.Detail)
		configdata.POST("/add", controller.ConfigData.Add)
		configdata.PUT("/update", controller.ConfigData.Update)
		configdata.DELETE("/delete/:id", controller.ConfigData.Delete)
		configdata.PUT("/status", controller.ConfigData.Status)
	}

	/* 网站设置 */
	configweb := e.Group("configweb")
	{
		configweb.GET("/index", controller.ConfigWeb.Index)
		configweb.PUT("/save", controller.ConfigWeb.Save)
	}

	/* 站点管理 */
	item := e.Group("item")
	{
		item.GET("/list", controller.Item.List)
		item.GET("/detail/:id", controller.Item.Detail)
		item.POST("/add", controller.Item.Add)
		item.PUT("/update", controller.Item.Update)
		item.DELETE("/delete/:id", controller.Item.Delete)
		item.PUT("/status", controller.Item.Status)
		item.GET("/getItemList", controller.Item.GetItemList)
	}

	/* 栏目管理 */
	itemcate := e.Group("itemcate")
	{
		itemcate.GET("/list", controller.ItemCate.List)
		itemcate.GET("/detail/:id", controller.ItemCate.Detail)
		itemcate.POST("/add", controller.ItemCate.Add)
		itemcate.PUT("/update", controller.ItemCate.Update)
		itemcate.DELETE("/delete/:id", controller.ItemCate.Delete)
		itemcate.GET("/getCateList", controller.ItemCate.GetCateList)
	}

	/* 广告位管理 */
	adsort := e.Group("adsort")
	{
		adsort.GET("/list", controller.AdSort.List)
		adsort.GET("/detail/:id", controller.AdSort.Detail)
		adsort.POST("/add", controller.AdSort.Add)
		adsort.PUT("/update", controller.AdSort.Update)
		adsort.DELETE("/delete/:id", controller.AdSort.Delete)
		adsort.GET("/getAdSortList", controller.AdSort.GetAdSortList)
	}

	/* 广告管理 */
	ad := e.Group("ad")
	{
		ad.GET("/list", controller.Ad.List)
		ad.GET("/detail/:id", controller.Ad.Detail)
		ad.POST("/add", controller.Ad.Add)
		ad.PUT("/update", controller.Ad.Update)
		ad.DELETE("/delete/:id", controller.Ad.Delete)
		ad.PUT("/status", controller.Ad.Status)
	}

	/* 代码生成器 */
	generate := e.Group("generate")
	{
		generate.GET("/list", controller.Generate.List)
		generate.POST("/generate", controller.Generate.Generate)
		generate.POST("/batchGenerate", controller.Generate.BatchGenerate)
	}

	/* 演示一 */
	example := e.Group("example")
	{
		example.GET("/list", controller.Example.List)
		example.GET("/detail/:id", controller.Example.Detail)
		example.POST("/add", controller.Example.Add)
		example.PUT("/update", controller.Example.Update)
		example.DELETE("/delete/:id", controller.Example.Delete)
		example.PUT("/status", controller.Example.Status)
		example.PUT("/setIsVip", controller.Example.IsVip)
	}

	/* 演示二 */
	example2 := e.Group("example2")
	{
		example2.GET("/list", controller.Example2.List)
		example2.GET("/detail/:id", controller.Example2.Detail)
		example2.POST("/add", controller.Example2.Add)
		example2.PUT("/update", controller.Example2.Update)
		example2.DELETE("/delete/:id", controller.Example2.Delete)
		example2.PUT("/status", controller.Example2.Status)
	}

	/* 文章 */
	article := e.Group("article")
	{
		article.GET("/list", controller.Article.List)
		article.GET("/detail/:id", controller.Article.Detail)
		article.POST("/add", controller.Article.Add)
		article.PUT("/update", controller.Article.Update)
		article.DELETE("/delete/{id:int}", controller.Article.Delete)
		article.PUT("/isTop", controller.Article.IsTop)
		article.PUT("/status", controller.Article.Status)
	}

}
